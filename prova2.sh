#!/bin/bash
opcao=""
menu () {
while true $opcao != "0"
do
clear
echo "===== Menu ====="
echo "1 - Verificar hardware do PC"
echo "2 - Verificar se um arquivo existe"
echo "3 - Exibir o diretorio atual"
echo "4 - Listar os arquivos do diretorio atual"
echo "5 - Mudar o diretorio atual"
echo "0 - Sair"
echo "Digite a opcao desejada:"
read opcao

case "$opcao" in
  1)
    clear
    echo "Exibindo informacoes do PC"
    cat /proc/cpuinfo
    sleep 10
  ;;
  2)
    clear
    echo "Informe o caminho do arquivo:"
    read caminho
    if [ -e "$caminho" ] ; then
      echo "O arquivo existe..."
    else
      echo "O arquivo nao existe..."
    fi
    sleep 5
  ;;
  3)
    clear
    echo "Exibindo o diretorio atual..."
    pwd
    sleep 5
  ;;
  4)
    clear
    echo "Exibindo os arquivos do diretorio atual..."
    ls -la
    sleep 5
  ;;
  5)
    clear
    echo "Informe o caminho de destino:"
    read caminho
    cd $caminho
    echo "Caminho atual:"
    pwd
    sleep 5
  ;;
  0)
    echo "Saindo..."
    clear;
    exit;
  ;;
  *)
    echo "Opcao invalida!!"
    sleep 5
esac
done
}
menu
